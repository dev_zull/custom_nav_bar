library custom_nav_bar;
import 'package:flutter/material.dart';

/// Material 3 Navigation Bar component.
///
/// Navigation bars offer a persistent and convenient way to switch between
/// primary destinations in an app.
///
/// This widget holds a collection of destinations (usually [NavDestination]s).
///
/// See also:
///
///  * [NavDestination]
///  * [NavigationDestination]
///  * [NavigationBar]
///  * [BottomNavigationBar]
///  * <https://m3.material.io/components/navigation-bar>
class SuperNavBar extends StatelessWidget {
  /// Creates a Material 3 Navigation Bar component.
  ///
  /// The value of [destinations] must be a list of two or more
  /// [NavDestination] values.
  const SuperNavBar({
    super.key,
    required this.destinations,
    this.selectedIndex = 0,
    this.backgroundColor,
    this.height = 64.0,
    this.type = MaterialType.canvas,
    this.elevation = 0.0,
    this.shadowColor,
    this.surfaceTintColor,
    this.borderRadius,
    this.navBarShape,
    this.borderOnForeground = true,
    this.clipBehavior = Clip.none,
    this.animationDuration = kThemeChangeDuration,
    this.navDestinationPosition = 0.0,
    this.selectedNavDestinationPosition = 5.0,
    this.animationCurve = Curves.easeIn,
    this.showIndicator = true,
    this.indicatorWidth = 64.0,
    this.indicatorHeight = 32.0,
    this.indicatorColor,
    this.indicatorOpacity = 0.3,
    this.indicatorBorderRadius,
    this.indicatorShape = BoxShape.rectangle,
    this.labelStyle,
    this.selectedLabelStyle = const TextStyle(fontWeight: FontWeight.w500),
    this.disabledLabelStyle = const TextStyle(color: Colors.grey),
    this.labelBehavior = LabelBehavior.alwaysShow,
    this.iconAndLabelGap,
    this.padding,
    this.margin,
    this.onDestinationSelected,
  })  : assert(destinations.length >= 2,
  'At least two destinations are required.'),
        assert(
        0 <= selectedIndex && selectedIndex < destinations.length,
        'The value of selectedIndex cannot be less than 0 or greater than '
            'the number of destinations.'),
        assert(0 <= indicatorOpacity && indicatorOpacity <= 1,
        'The indicator opacity must not be greater than 1 or negative.');

  /// The list of [NavDestination] widgets for the navigation bar.
  final List<NavDestination> destinations;

  /// The index of the currently selected [NavDestination].
  final int selectedIndex;

  /// The background color of the navigation bar.
  final Color? backgroundColor;

  /// The height of the navigation bar.
  final double height;

  /// The kind of material to show (e.g., card or canvas) for the navigation
  /// bar. This affects the shape of the navigation bar, the roundness of its
  /// corners if the shape is rectangular, and the default color.
  final MaterialType type;

  /// The z-coordinate at which to place the navigation bar relative to its
  /// parent.
  ///
  /// This controls the size of the shadow below the navigation bar and the
  /// opacity of the elevation overlay color if it is applied.
  ///
  /// If this is non-zero, the contents of the navigation bar are clipped,
  /// because the widget conceptually defines an independent printed piece of
  /// material.
  ///
  /// Defaults to 0. Changing this value will cause the shadow and the elevation
  /// overlay or surface tint to animate over [Material.animationDuration].
  ///
  /// The value is non-negative.
  final double elevation;

  /// The color to paint the shadow below the navigation bar.
  ///
  /// If null and [ThemeData.useMaterial3] is true then [ThemeData]'s
  /// [ColorScheme.shadow] will be used. If [ThemeData.useMaterial3] is false
  /// then [ThemeData.shadowColor] will be used.
  ///
  /// To remove the drop shadow when [elevation] is greater than 0, set
  /// [shadowColor] to [Colors.transparent].
  final Color? shadowColor;

  /// The color of the surface tint overlay applied to the [backgroundColor] of
  /// the navigation bar to indicate elevation.
  ///
  /// Material Design 3 introduced a new way for some components to indicate
  /// their elevation by using a surface tint color overlay on top of the
  /// base material [color]. This overlay is painted with an opacity that is
  /// related to the [elevation] of the material.
  ///
  /// If [ThemeData.useMaterial3] is false, then this property is not used.
  ///
  /// If [ThemeData.useMaterial3] is true and [surfaceTintColor] is not null and
  /// not [Colors.transparent], then it will be used to overlay the base [color]
  /// with an opacity based on the [elevation].
  ///
  /// Otherwise, no surface tint will be applied.
  final Color? surfaceTintColor;

  /// If non-null, the corners of navigation bar are rounded by this
  /// [BorderRadiusGeometry] value.
  ///
  /// Otherwise, the corners specified for the current [type] of material are
  /// used.
  ///
  /// If [navBarShape] is non null then the border radius is ignored.
  ///
  /// Must be null if [type] is [MaterialType.circle].
  final BorderRadiusGeometry? borderRadius;

  /// Defines the shape of the navigation bar as well its shadow.
  ///
  /// If [navBarShape] is non null, the [borderRadius] is ignored and the clip
  /// boundary and shadow of the navigation bar are defined by the
  /// [navBarShape].
  ///
  /// A shadow is only displayed if the [elevation] is greater than
  /// zero.
  final ShapeBorder? navBarShape;

  /// Whether to paint the [navBarShape] border in front of the navigation bar.
  ///
  /// The default value is true.
  /// If false, the border will be painted behind the navigation bar.
  final bool borderOnForeground;

  /// The content will be clipped (or not) according to this option.
  ///
  /// See the enum [Clip] for details of all possible options and their common
  /// use cases.
  ///
  /// Defaults to [Clip.none].
  final Clip clipBehavior;

  /// [NavDestination] distance from the bottom of the navigation bar when it
  /// is not selected.
  ///
  /// Effective only when the [labelBehavior] is set to
  /// [LabelBehavior.onlyShowSelected].
  ///
  /// The default value is 0.0.
  final double navDestinationPosition;

  /// [NavDestination] distance from the bottom of the navigation bar when it
  /// is selected.
  ///
  /// Effective only when the [labelBehavior] is set to
  /// [LabelBehavior.onlyShowSelected].
  ///
  /// The default value is 5.0.
  final double selectedNavDestinationPosition;

  /// For each of the [NavDestination] in the [destinations] list it defines
  /// the duration of animated changes for the indicator, label opacity (when
  /// label behavior is [LabelBehavior.onlyShowSelected]) and the movement of
  /// the [NavDestination] (when label behavior is
  /// [LabelBehavior.onlyShowSelected]).
  ///
  /// It also defines the duration of the animated changes for [navBarShape],
  /// [elevation], [shadowColor], [surfaceTintColor] and the elevation overlay
  /// if it is applied.
  ///
  /// The default value is [kThemeChangeDuration].
  final Duration animationDuration;

  /// Defines the animation curve for the indicator animation, [NavDestination]
  /// movement (when label behavior is [LabelBehavior.onlyShowSelected])
  final Curve animationCurve;

  /// Specifies whether to show the indicator.
  ///
  /// If false, the [indicatorWidth] and the [indicatorHeight] are always set
  /// to their default values.
  ///
  /// The default value is true.
  final bool showIndicator;

  /// Specifies the width of the indicator.
  ///
  /// It's always set to its default value if you set the value of
  /// [showIndicator] to false.
  ///
  /// The default value is 64.0.
  final double indicatorWidth;

  /// Specifies the height of the indicator.
  ///
  /// It's always set to its default value if you set the value of
  /// [showIndicator] to false.
  ///
  /// The default value is 32.0.
  final double indicatorHeight;

  /// Specifies the color of the indicator.
  final Color? indicatorColor;

  /// Specifies the opacity of the indicator.
  final double indicatorOpacity;

  /// If non-null, the corners of the indicator are rounded by the
  /// [indicatorBorderRadius].
  ///
  /// Applies only if the indicators are with rectangular shapes; ignored if
  /// [indicatorShape] is not [BoxShape.rectangle].
  final BorderRadiusGeometry? indicatorBorderRadius;

  /// The shape to fill the [indicatorColor] into and to cast as the
  /// [boxShadow].
  ///
  /// If this is [BoxShape.circle] then [indicatorBorderRadius] is ignored.
  final BoxShape indicatorShape;

  /// Specifies the [TextStyle] for the unselected destination labels.
  final TextStyle? labelStyle;

  /// Specifies the [TextStyle] for the selected destination label.
  ///
  /// If null, the [labelStyle] is used.
  final TextStyle? selectedLabelStyle;

  /// Specifies the [TextStyle] for the disabled destination labels.
  ///
  /// If null, the [labelStyle] is used.
  ///
  /// For disabling a navigation destination set the [NavDestination.enabled]
  /// property to false.
  final TextStyle? disabledLabelStyle;

  /// Defines how the [destinations]' labels will be laid out and when they'll
  /// be displayed.
  ///
  /// Can be used to show all labels, show only the selected label, or hide all
  /// labels.
  ///
  /// The default behaviour is [LabelBehavior.alwaysShow]
  final LabelBehavior? labelBehavior;

  /// Specifies the amount of gap between the icon and the label of a
  /// [NavDestination].
  ///
  /// If the [labelBehavior] is set to [LabelBehavior.alwaysHide], no gap is
  /// created between the [NavDestination]'s icon and the label.
  final double? iconAndLabelGap;

  /// Empty space to inscribe inside the [SuperNavBar]. The [NavDestination]s,
  /// are placed inside this padding.
  ///
  /// This padding is in addition to any padding inherent in the [SuperNavBar].
  final EdgeInsetsGeometry? padding;

  /// Empty space to surround the [SuperNavBar] and the [NavDestination]s.
  final EdgeInsetsGeometry? margin;

  /// Called when one of the [destinations] is selected.
  ///
  /// This callback usually updates the int passed to [selectedIndex].
  ///
  /// Upon updating [selectedIndex], the [SuperNavBar] will be rebuilt.
  final ValueChanged<int>? onDestinationSelected;

  @override
  Widget build(BuildContext context) {
    return Material(
      type: type,
      elevation: elevation,
      color: backgroundColor ??
          Theme.of(context).navigationBarTheme.backgroundColor ??
          const Color(0xfff6f2fd),
      shadowColor: shadowColor,
      surfaceTintColor: surfaceTintColor,
      textStyle: labelStyle,
      borderRadius: borderRadius,
      shape: navBarShape,
      borderOnForeground: borderOnForeground,
      clipBehavior: clipBehavior,
      animationDuration: animationDuration,
      child: SafeArea(
        child: Container(
          height: height,
          padding: padding,
          margin: margin,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              for (int i = 0; i < destinations.length; i++)
                Expanded(
                  child: NavDestinationInfo(
                    index: i,
                    selectedIndex: selectedIndex,
                    destination: destinations[i],
                    navDestinationPosition: navDestinationPosition,
                    selectedNavDestinationPosition:
                    selectedNavDestinationPosition,
                    animationDuration: animationDuration,
                    animationCurve: animationCurve,
                    showIndicator: showIndicator,
                    indicatorWidth: showIndicator ? indicatorWidth : 64.0,
                    indicatorHeight: showIndicator ? indicatorHeight : 32.0,
                    indicatorColor: indicatorColor ??
                        Theme.of(context).navigationBarTheme.indicatorColor ??
                        const Color(0xffbcb1da),
                    indicatorOpacity: indicatorOpacity,
                    indicatorBorderRadius: indicatorShape == BoxShape.rectangle
                        ? indicatorBorderRadius ?? BorderRadius.circular(16)
                        : null,
                    indicatorShape: indicatorShape,
                    selectedLabelStyle: selectedLabelStyle,
                    disabledLabelStyle: disabledLabelStyle,
                    labelBehavior: labelBehavior,
                    iconAndLabelGap: iconAndLabelGap,
                    onDestinationTapped: (index) {
                      onDestinationSelected != null
                          ? onDestinationSelected!(index)
                          : () {};
                    },
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}

/// Specifies when each [NavDestination]'s label should appear.
///
/// This is used to determine the behavior of [SuperNavBar]'s destinations.
enum LabelBehavior {
  /// All of the labels are always visible.
  alwaysShow,

  /// Labels are never visible.
  alwaysHide,

  /// Only the label for the selected navigation destination is visible.
  ///
  /// On selection change of the navigation destinations the destinations
  /// slightly animate.
  onlyShowSelected,
}

/// A Material 3 [SuperNavBar] destination.
///
/// Displays a label below an icon. Use with [SuperNavBar.destinations].
class NavDestination {
  /// Creates a navigation bar destination with an icon and a label, to be used
  /// in the [SuperNavBar.destinations].
  const NavDestination({
    required this.icon,
    this.selectedIcon,
    this.disabledIcon,
    required this.label,
    this.tooltip,
    this.enabled = true,
  });

  /// The icon displayed in the destination.
  final Widget icon;

  /// The icon to be displayed when the destination is selected.
  ///
  /// If null, [icon] is used.
  final Widget? selectedIcon;

  /// The icon to be displayed if the destination is disabled.
  ///
  /// Set the [enabled] property to false to disable the destination.
  ///
  /// If null, [icon] is used.
  final Widget? disabledIcon;

  /// The label to be displayed below the destination icon.
  final String label;

  /// The tooltip to be displayed to the user when the destination is long
  /// pressed.
  ///
  /// If null, the [label] is used as the tooltip message.
  ///
  /// If it is set to an empty string '', no tooltip is displayed.
  final String? tooltip;

  /// Specifies whether the destination is enabled.
  ///
  /// If disabled, the destination cannot be selected.
  ///
  /// The default value is true.
  final bool enabled;
}

class NavDestinationInfo extends StatelessWidget {
  const NavDestinationInfo({
    super.key,
    required this.index,
    required this.selectedIndex,
    required this.destination,
    required this.navDestinationPosition,
    required this.selectedNavDestinationPosition,
    required this.animationDuration,
    required this.animationCurve,
    required this.showIndicator,
    required this.indicatorWidth,
    required this.indicatorHeight,
    required this.indicatorColor,
    required this.indicatorOpacity,
    required this.indicatorShape,
    this.indicatorBorderRadius,
    this.selectedLabelStyle,
    this.disabledLabelStyle,
    this.labelBehavior,
    this.iconAndLabelGap,
    required this.onDestinationTapped,
  });

  /// Index of this navigation destination.
  final int index;

  /// Index of the selected navigation destination.
  final int selectedIndex;

  final NavDestination destination;

  /// [NavDestination] distance from the bottom of the navigation bar when it
  /// is not selected.
  ///
  /// Effective only when the [labelBehavior] is set to
  /// [LabelBehavior.onlyShowSelected].
  final double navDestinationPosition;

  /// [NavDestination] distance from the bottom of the navigation bar when it
  /// is selected.
  ///
  /// Effective only when the [labelBehavior] is set to
  /// [LabelBehavior.onlyShowSelected].
  final double selectedNavDestinationPosition;
  final Duration animationDuration;
  final Curve animationCurve;
  final bool showIndicator;
  final double indicatorWidth;
  final double indicatorHeight;
  final Color? indicatorColor;
  final double indicatorOpacity;
  final BoxShape indicatorShape;
  final BorderRadiusGeometry? indicatorBorderRadius;
  final TextStyle? selectedLabelStyle;
  final TextStyle? disabledLabelStyle;
  final LabelBehavior? labelBehavior;
  final double? iconAndLabelGap;
  final ValueChanged<int>? onDestinationTapped;

  @override
  Widget build(BuildContext context) {
    Widget destinationWidget;
    if (labelBehavior != LabelBehavior.onlyShowSelected) {
      destinationWidget = Tooltip(
        message: destination.tooltip ?? destination.label,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            InkWell(
              onTap: destination.enabled
                  ? () {
                onDestinationTapped != null
                    ? onDestinationTapped!(index)
                    : () {};
              }
                  : null,
              borderRadius: indicatorBorderRadius?.resolve(TextDirection.ltr),

              // Indicator
              child: AnimatedContainer(
                width: indicatorWidth,
                height: indicatorHeight,
                alignment: Alignment.center,
                decoration: showIndicator && index == selectedIndex
                    ? BoxDecoration(
                  color: indicatorColor?.withOpacity(indicatorOpacity),
                  borderRadius: indicatorBorderRadius,
                  shape: indicatorShape,
                )
                    : const BoxDecoration(),
                duration: animationDuration,
                curve: animationCurve,
                child: destination.enabled
                    ? (index == selectedIndex
                    ? destination.selectedIcon ?? destination.icon
                    : destination.icon)
                    : destination.disabledIcon ?? destination.icon,
              ),
            ),

            // If the labelBehavior is LabelBehavior.alwaysHide, no label widget
            // and no gap between the icon and the label is rendered.
            if (labelBehavior == LabelBehavior.alwaysShow) ...[
              if (iconAndLabelGap != null) SizedBox(height: iconAndLabelGap),
              // Make the label clickable
              InkWell(
                onTap: destination.enabled
                    ? () {
                  onDestinationTapped != null
                      ? onDestinationTapped!(index)
                      : () {};
                }
                    : null,

                // Label
                child: Text(
                  destination.label,
                  // When the style is null, the labelStyle is applied
                  // automatically which is passed to the Material widget
                  // above.
                  style: destination.enabled
                      ? (index == selectedIndex ? selectedLabelStyle : null)
                      : disabledLabelStyle,
                ),
              ),
            ],
          ],
        ),
      );
    }
    // When the labelBehavior is set to LabelBehavior.onlyShowSelected
    else {
      destinationWidget = Stack(
        alignment: Alignment.center,
        children: [
          AnimatedPositioned(
            bottom: index == selectedIndex
                ? selectedNavDestinationPosition
                : navDestinationPosition,
            duration: animationDuration,
            curve: animationCurve,
            child: Tooltip(
              message: destination.tooltip ?? destination.label,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  InkWell(
                    onTap: destination.enabled
                        ? () {
                      onDestinationTapped != null
                          ? onDestinationTapped!(index)
                          : () {};
                    }
                        : null,
                    borderRadius:
                    indicatorBorderRadius?.resolve(TextDirection.ltr),

                    // Indicator
                    child: AnimatedContainer(
                      width: indicatorWidth,
                      height: indicatorHeight,
                      alignment: Alignment.center,
                      decoration: showIndicator && index == selectedIndex
                          ? BoxDecoration(
                        color:
                        indicatorColor?.withOpacity(indicatorOpacity),
                        borderRadius: indicatorBorderRadius,
                        shape: indicatorShape,
                      )
                          : const BoxDecoration(),
                      duration: animationDuration,
                      curve: animationCurve,
                      child: destination.enabled
                          ? (index == selectedIndex
                          ? destination.selectedIcon ?? destination.icon
                          : destination.icon)
                          : destination.disabledIcon ?? destination.icon,
                    ),
                  ),
                  if (iconAndLabelGap != null)
                    SizedBox(height: iconAndLabelGap),
                  AnimatedOpacity(
                    opacity: index == selectedIndex ? 1 : 0,
                    duration: animationDuration,

                    // Label
                    child: Text(
                      destination.label,
                      // When the style is null, the labelStyle is applied
                      // automatically which is passed to the Material widget.
                      style: destination.enabled
                          ? (index == selectedIndex ? selectedLabelStyle : null)
                          : disabledLabelStyle,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      );
    }

    return destinationWidget;
  }
}

